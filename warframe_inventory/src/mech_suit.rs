use super::ItemId;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct MechSuit {
    /// Name
    pub item_type: String,

    // pub configs: Vec<Config>,
    // pub upgrade_ver: Option<i64>,
    /// XP Value
    #[serde(rename = "XP")]
    #[serde(default)]
    pub xp: i32,

    pub item_id: ItemId,
}
