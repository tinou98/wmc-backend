use serde::{Deserialize, Serialize};
use serde_json::{from_reader, Result};
use std::collections::HashMap;
use std::io::BufReader;
use std::path::Path;
use std::{fs::File, num::NonZeroU32};

mod warframe;
pub use warframe::Suit;

mod long_gun;
pub use long_gun::LongGun;

mod mech_suit;
use mech_suit::MechSuit;

mod utils;
pub use utils::{Created, Date, ItemId};

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Bin {
    #[serde(default)]
    pub extra: i64,
    pub slots: i64,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct MiscItem {
    pub item_count: u32,
    pub item_type: String,
}

// lore_fragment scan
// PendingRecipes
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Inventory {
    /*
    pub subscribed_to_emails: i64,
    pub created: Created,
    pub reward_seed: i64,
    pub regular_credits: i64,
    pub premium_credits: i64,
    pub premium_credits_free: i64,
    pub fusion_points: i64,
    */
    pub suit_bin: Bin,
    pub weapon_bin: Bin,
    pub sentinel_bin: Bin,
    pub space_suit_bin: Bin,
    pub space_weapon_bin: Bin,
    pub pvp_bonus_loadout_bin: Bin,
    pub pve_bonus_loadout_bin: Bin,
    pub random_mod_bin: Bin,
    /*
    pub version: i64,
    pub trades_remaining: i64,
    pub daily_affiliation: i64,
    pub daily_affiliation_pvp: i64,
    pub daily_affiliation_library: i64,
    pub daily_focus: i64,
    pub gifts_remaining: i64,
    pub handler_points: i64,
    */
    pub misc_items: Vec<MiscItem>,
    /*
    pub challenges_fix_version: i64,
    // pub challenge_progress: Vec<ChallengeProgress>,
    // pub raw_upgrades: Vec<RawUpgrade>,
    pub received_starting_gear: bool,
    */
    pub suits: Vec<Suit>,
    pub long_guns: Vec<LongGun>,
    pub pistols: Vec<LongGun>,
    pub melee: Vec<LongGun>,
    // pub pistols: Vec<Pistol>,
    // pub melee: Vec<Melee>,
    /*
    pub ships: Vec<Ship>,
    pub quest_keys: Vec<QuestKey>,
    pub flavour_items: Vec<FlavourItem>,
    pub scoops: Vec<Scoop>,
    pub training_retries_left: i64,
    pub ship: Ship2,
    pub load_out_presets: LoadOutPresets,
    pub current_load_out_ids: Vec<CurrentLoadOutId>,
    pub missions: Vec<Mission>,
    pub random_upgrades_identified: i64,
    pub last_region_played: String,
    */
    #[serde(rename = "XPInfo")]
    pub xpinfo: Vec<XpInfo>,
    pub recipes: Vec<MiscItem>,
    /*
    pub guild_id: GuildId,
    pub boosters: Vec<Booster>,
    pub weapon_skins: Vec<WeaponSkin>,
    pub completed_alerts: Vec<String>,
    pub training_date: TrainingDate,
    pub player_level: i64,
    pub personal_goal_progress: Vec<PersonalGoalProgress>,
    pub affiliations: Vec<Affiliation>,
    pub challenge_instance_states: Vec<ChallengeInstanceState>,
    pub bounty_score: i64,
    pub level_keys: Vec<LevelKey>,
    pub upgrades: Vec<Upgrade>,
    pub has_contributed_to_dojo: bool,
    pub pending_trades: Vec<::serde_json::Value>,
    pub death_marks: Vec<String>,
    pub consumables: Vec<Consumable>,
    pub space_suits: Vec<SpaceSuit>,
    pub space_melee: Vec<SpaceMelee>,
    pub space_guns: Vec<SpaceGun>,
    pub archwing_enabled: bool,
    pub sentient_spawn_chance_boosters: SentientSpawnChanceBoosters,
    pub periodic_mission_completions: Vec<PeriodicMissionCompletion>,
    pub equipped_gear: Vec<String>,
    pub lore_fragment_scans: Vec<LoreFragmentScan>,
    pub drones: Vec<Drone>,
    pub faction_scores: Vec<i64>,
    pub qualifying_invasions: Vec<::serde_json::Value>,
    pub kubrow_pet_eggs: Vec<::serde_json::Value>,
    pub email_items: Vec<EmailItem>,
    pub kubrow_pets: Vec<KubrowPet>,
    pub wishlist: Vec<::serde_json::Value>,
    pub web_flags: WebFlags,
    pub pending_spectre_loadouts: Vec<::serde_json::Value>,
    pub spectre_loadouts: Vec<SpectreLoadout>,
    pub completed_syndicates: Vec<String>,
    #[serde(rename = "FocusXP")]
    pub focus_xp: FocusXp,
    pub fusion_treasures: Vec<FusionTreasure>,
    pub taunt_history: Vec<TauntHistory>,
    pub ship_decorations: Vec<ShipDecoration>,
    pub alignment: Alignment,
    pub completed_sorties: Vec<String>,
    pub sentinel_weapons: Vec<SentinelWeapon>,
    pub sentinels: Vec<Sentinel2>,
    pub active_dojo_color_research: String,
    pub last_sortie_reward: Vec<LastSortieReward>,
    pub equipped_emotes: Vec<String>,
    pub branded_suits: Vec<::serde_json::Value>,
    */
    pub operator_amp_bin: Bin,
    /*
    pub daily_affiliation_cetus: i64,
    pub daily_affiliation_quills: i64,
    pub focus_ability: String,
    pub focus_upgrades: Vec<FocusUpgrade>,
    pub discovered_markers: Vec<DiscoveredMarker>,
    pub completed_jobs: Vec<CompletedJob>,
    pub operator_amps: Vec<OperatorAmp>,
    pub focus_capacity: i64,
    pub daily_affiliation_solaris: i64,
    pub subscribed_to_emails_personalized: i64,
    pub special_items: Vec<SpecialItem>,
    pub login_milestone_rewards: Vec<String>,
    pub operator_load_outs: Vec<OperatorLoadOut>,
    pub daily_affiliation_ventkids: i64,
    pub daily_affiliation_vox: i64,
    pub recent_vendor_purchases: Vec<::serde_json::Value>,
    pub node_intros_completed: Vec<String>,
    pub season_challenge_history: Vec<SeasonChallengeHistory>,
    */
    pub pending_recipes: Vec<PendingRecipe>,
    /*
    #[serde(rename = "HWIDProtectEnabled")]
    pub hwidprotect_enabled: bool,
    pub moa_pets: Vec<MoaPet>,
    pub invasion_chain_progress: Vec<InvasionChainProgress>,
    pub data_knives: Vec<DataKnfe>,
    pub personal_tech_projects: Vec<PersonalTechProject>,
    */
    pub crew_ship_salvage_bin: Bin,
    /*
    pub settings: Settings,
    pub played_parkour_tutorial: bool,
    pub crew_ships: Vec<CrewShip>,
    pub crew_ship_ammo: Vec<CrewShipAmmo>,
    pub player_skills: PlayerSkills,
    pub crew_ship_salvaged_weapon_skins: Vec<CrewShipSalvagedWeaponSkin>,
    pub crew_ship_fusion_points: i64,
    pub crew_ship_salvaged_weapons: Vec<CrewShipSalvagedWeapon>,
    pub nemesis_history: Vec<NemesisHistory>,
    pub crew_ship_weapon_skins: Vec<CrewShipWeaponSkin>,
    pub last_nemesis_ally_spawn_time: LastNemesisAllySpawnTime,
    */
    pub mech_bin: Bin,
    /*
    pub daily_affiliation_entrati: i64,
    pub daily_affiliation_necraloid: i64,
    pub crew_ship_weapons: Vec<CrewShipWeapon>,
    pub completed_job_chains: Vec<CompletedJobChain>,
    pub last_inventory_sync: LastInventorySync,
    pub next_refill: NextRefill,
    pub active_landscape_traps: Vec<::serde_json::Value>,
    */
    pub mech_suits: Vec<MechSuit>,
    /*
    pub crew_members: Vec<::serde_json::Value>,
    pub hoverboards: Vec<::serde_json::Value>,
    pub kubrow_pet_prints: Vec<::serde_json::Value>,
    pub rep_votes: Vec<::serde_json::Value>,
    pub step_sequencers: Vec<::serde_json::Value>,
    pub league_tickets: Vec<::serde_json::Value>,
    pub quests: Vec<::serde_json::Value>,
    pub robotics: Vec<::serde_json::Value>,
    pub used_daily_deals: Vec<::serde_json::Value>,
    pub library_personal_progress: Vec<::serde_json::Value>,
    pub collectible_series: Vec<CollectibleSery>,
    pub library_available_daily_task_info: LibraryAvailableDailyTaskInfo,
    pub has_reset_account: bool,
    pub pending_coupon: PendingCoupon,
    pub harvestable: bool,
    pub death_squadable: bool,
    */
    #[serde(flatten)]
    _other: HashMap<String, ::serde_json::Value>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct XpInfo {
    item_type: String,

    #[serde(rename = "XP")]
    pub xp: u32,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct PendingRecipe {
    pub item_type: String,
    pub completion_date: Date,
    pub item_id: ItemId,
}

impl Inventory {
    pub fn load_file<P: AsRef<Path>>(path: P) -> Result<Self> {
        let f = File::open(path).unwrap();
        let f = BufReader::new(f);

        from_reader(f)
    }

    pub fn has(&self, name: &str) -> bool {
        self.find(name).next().is_some()
    }

    pub fn find<'a>(&'a self, name: &'a str) -> impl Iterator<Item = &str> + 'a {
        std::iter::empty()
            .chain(self.suits.iter().map(|s| s.item_type.as_str()))
            .chain(self.long_guns.iter().map(|s| s.item_type.as_str()))
            .chain(self.pistols.iter().map(|s| s.item_type.as_str()))
            .chain(self.melee.iter().map(|s| s.item_type.as_str()))
            .chain(self.mech_suits.iter().map(|s| s.item_type.as_str()))
            .filter(move |n| n == &name)
    }

    pub fn xp(&self, name: &str) -> u32 {
        self.xpinfo
            .iter()
            .find(|xp_info| xp_info.item_type == name)
            .map_or(0, |xp_info| xp_info.xp)
    }

    pub fn has_enough_item(&self, name: &str, qte: u32) -> bool {
        self.num_missing_item(name, qte).is_none()
    }

    pub fn num_item(&self, name: &str) -> Option<NonZeroU32> {
        self.misc_items
            .iter()
            .chain(self.recipes.iter())
            .find(|item| item.item_type == name)
            .and_then(|item| NonZeroU32::new(item.item_count))
    }

    pub fn num_missing_item(&self, name: &str, wanted_qte: u32) -> Option<NonZeroU32> {
        wanted_qte
            .checked_sub(self.num_item(name).map_or(0, u32::from))
            .and_then(NonZeroU32::new)
    }

    pub fn has_building(&self, name: &str) -> Option<&PendingRecipe> {
        self.pending_recipes
            .iter()
            .find(|item| item.item_type == name)
    }
}
