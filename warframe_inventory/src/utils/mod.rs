use serde::{Deserialize, Serialize};

mod date;
pub use date::Date;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Created {
    #[serde(rename = "$date")]
    pub date: Date,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct ItemId {
    #[serde(rename = "$oid")]
    pub oid: String,
}
