use anyhow::{Context, Result};
use std::{
    collections::HashMap as Map,
    ops::Deref,
    path::{Path, PathBuf},
    sync::Arc,
};
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt},
    sync::{Notify, OwnedRwLockReadGuard, RwLock},
};
use tracing::error;
use warframe_inventory::Inventory;

pub type UserDataMap = Map<String, Inventory>;
pub type LockHandle<T> = OwnedRwLockReadGuard<UserDataMap, T>;
pub type InventoryHandle = LockHandle<Inventory>;

#[derive(Debug, Clone)]
pub struct UserData {
    data: Arc<RwLock<UserDataMap>>,
    file: PathBuf,
    changed: Arc<Notify>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum InventoryUpdateStatus {
    Updated,
    Created,
}

impl UserData {
    pub fn new(data_folder: &Path) -> Self {
        let s = Self {
            data: Arc::default(),
            file: data_folder.join("userdata.json"),
            changed: Arc::default(),
        };
        let s2 = s.clone();
        tokio::spawn(async move {
            loop {
                s2.changed.notified().await;
                if let Err(err) = s2.save().await {
                    error!(err = ?err, "Unable to save user data")
                }
            }
        });
        s
    }

    pub async fn read_user(&self, mail: &str) -> Option<InventoryHandle> {
        let r = Arc::clone(&self.data).read_owned().await;
        OwnedRwLockReadGuard::try_map(r, |e| e.get(mail)).ok()
    }

    pub async fn add_user(&self, mail: String, inventory: Inventory) -> InventoryUpdateStatus {
        let status = match self.data.write().await.insert(mail, inventory) {
            Some(_) => InventoryUpdateStatus::Updated,
            None => InventoryUpdateStatus::Created,
        };
        self.changed.notify_one();
        status
    }

    pub async fn user_list(&self) -> Vec<String> {
        self.data.read().await.keys().cloned().collect()
    }

    async fn save(&self) -> Result<()> {
        let mut file = File::create(&self.file)
            .await
            .with_context(|| format!("Can't open user_data file {:?}", self.file))?;

        let s = serde_json::to_string_pretty(self.data.read().await.deref())
            .context("Can't serialize user datat state")?;

        file.write_all(s.as_bytes())
            .await
            .with_context(|| format!("Can't write to user_data file {:?}", self.file))?;

        Ok(())
    }

    pub async fn load(&self) -> Result<bool> {
        use std::io::ErrorKind::NotFound;
        let mut file = match File::open(&self.file).await {
            Ok(f) => f,
            Err(err) if err.kind() == NotFound => return Ok(false),
            Err(err) => {
                Err(err).with_context(|| format!("Can't open user_data file {:?}", self.file))?
            }
        };

        let mut data = Vec::new();
        file.read_to_end(&mut data)
            .await
            .with_context(|| format!("Can't read user_data file {:?}", self.file))?;

        let data = serde_json::from_slice(&data)
            .with_context(|| format!("Can't parse user_data file {:?}", self.file))?;

        *self.data.write().await = data;

        Ok(true)
    }

    pub fn file_path(&self) -> &PathBuf {
        &self.file
    }
}
