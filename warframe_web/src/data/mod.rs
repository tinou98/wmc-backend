use arc_swap::{ArcSwap, Guard};
use axum::extract::FromRef;
use std::{path::PathBuf, sync::Arc};
use tracing::{error, info, warn};
use warframe_exports::Exports;

pub mod user_data;
use user_data::UserData;

#[derive(Clone)]
pub struct AppState(Arc<State>);

impl AppState {
    pub async fn new() -> Self {
        info!("Preparing users datas");
        let data_folder = std::env::var_os("STATE_DIRECTORY")
            .map_or_else(|| PathBuf::new().join("data"), PathBuf::from);
        tokio::fs::create_dir_all(&data_folder).await.unwrap();

        let user_data = user_data::UserData::new(&data_folder);
        match user_data.load().await {
            Ok(true) => (),
            Ok(false) => {
                warn!(
                    path = ?user_data.file_path(),
                    "Unable to load user data: no such file",
                )
            }
            Err(err) => {
                error!(?err, "Unable to load user data")
            }
        };
        info!("Processing exports");
        let exports = ArcSwap::from_pointee(
            Exports::new(data_folder.join("last_exports"))
                .await
                .unwrap(),
        );
        info!("Datas ready");

        Self(Arc::new(State { exports, user_data }))
    }

    pub fn user_data(&self) -> &UserData {
        &self.0.user_data
    }

    pub fn exports(&self) -> &ArcSwap<Exports> {
        &self.0.exports
    }
}

pub type Handle<T> = Guard<Arc<T>>;
pub type HExports = Handle<Exports>;

struct State {
    exports: ArcSwap<Exports>,
    user_data: UserData,
}

impl FromRef<AppState> for Handle<Exports> {
    fn from_ref(input: &AppState) -> Self {
        input.0.exports.load()
    }
}

impl FromRef<AppState> for Arc<State> {
    fn from_ref(input: &AppState) -> Self {
        Arc::clone(&input.0)
    }
}
