use chrono::{DateTime, Local};
use warframe_exports::{Exports, XPItem};
use warframe_inventory::Inventory;

pub struct OwnStatus<'a> {
    owned: bool,
    own_better: Vec<&'a dyn XPItem>,
    lvl: u8,
    lvl_max: bool,
    crafting: Option<DateTime<Local>>,
}

impl<'a> OwnStatus<'a> {
    pub fn new(item: &dyn XPItem, api: &Inventory, exports: &'a Exports) -> Self {
        Self {
            owned: api.has(item.unique_name()),
            own_better: item
                .get_prime(exports)
                .into_iter()
                .filter(|w| api.has(w.unique_name()))
                .collect(),
            lvl: item.level(api.xp(item.unique_name())),
            lvl_max: item.is_done(api.xp(item.unique_name())),
            crafting: exports
                .recipe_for(item.unique_name())
                .flat_map(|r| api.has_building(&r.unique_name))
                .next()
                .map(|bld| bld.completion_date.date),
        }
    }

    pub fn owned(&self) -> bool {
        self.owned
    }

    pub fn own_better(&self) -> bool {
        !self.own_better.is_empty()
    }

    pub fn better(&'a self) -> impl Iterator<Item = &'a dyn XPItem> + 'a {
        self.own_better.iter().cloned()
    }

    pub fn level(&self) -> u8 {
        self.lvl
    }

    pub fn lvl_max(&self) -> bool {
        self.lvl_max
    }

    pub fn craft_required(&self) -> bool {
        self.status() == Status::CraftRequired
    }

    pub fn status(&self) -> Status {
        match (
            self.lvl_max(),
            self.owned(),
            self.own_better(),
            self.crafting,
        ) {
            (false, true, _, _) => Status::XpRequired,
            (false, false, _, Some(d)) => Status::Crafting(d),
            (false, false, _, None) => Status::CraftRequired,
            (true, true, true, _) => Status::BetterVersion,
            (true, _, _, _) => Status::Ok,
        }
    }
}

#[derive(Debug, Eq, PartialEq, serde::Serialize)]
pub enum Status {
    /// Everithing is good with this item
    Ok,

    /// This Warframe need to level up
    XpRequired,

    /// A better version is owned
    BetterVersion,

    /// Item is Crafting
    Crafting(DateTime<Local>),

    /// Crafting is required
    CraftRequired,
}
