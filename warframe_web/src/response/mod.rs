use axum::{response::IntoResponse, Json};
use serde::Serialize;

pub mod craft_info;
pub mod flat_prerequist;
pub mod item;

#[derive(Debug, Serialize)]
pub struct Response<T: Serialize> {
    data: T,
    // object_info: Map<String, ()>,
}

impl<T: Serialize> Response<T> {
    pub fn new(data: T) -> Self {
        Response { data }
    }
}

/*
impl<'r, T: Serialize> Responder<'r, 'static> for Response<T> {
    fn respond_to(self, request: &'r rocket::Request<'_>) -> rocket::response::Result<'static> {
        Json(self).respond_to(request)
    }
}
*/

impl<T: Serialize> IntoResponse for Response<T> {
    fn into_response(self) -> axum::response::Response {
        Json(self).into_response()
    }
}
