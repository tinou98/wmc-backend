use super::Response;
use serde::Serialize;
use warframe_exports::{Exports, Item, ItemKind};

pub type ItemList<Data> = Response<Vec<ItemInfo<Data>>>;

#[derive(Debug, Serialize)]
pub struct ItemInfo<Data: Serialize> {
    name: String,
    unique_name: String,
    description: String,
    image_url: String,
    kind: ItemKind,

    // #[serde(flatten)]
    data: Data,
}

impl<Data: Serialize> ItemInfo<Data> {
    pub fn new(item: &dyn Item, data: Data, exports: &Exports) -> Self {
        Self {
            name: item.name().to_string(),
            unique_name: item.unique_name().to_string(),
            description: item.description().to_string(),
            image_url: format!(
                "https://content.warframe.com/PublicExport/{}",
                exports.get_image(item).unwrap()
            ),
            kind: item.kind(),

            data,
        }
    }

    pub fn from_name(item_name: &str, data: Data, exports: &Exports) -> Option<Self> {
        exports
            .items()
            .find(|e| e.unique_name() == item_name)
            .map(|item| Self::new(item, data, exports))
    }
}
