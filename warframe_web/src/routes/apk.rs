use axum::{headers::ContentType, response::IntoResponse, routing::get, Router, TypedHeader};
use mime::Mime;

pub(super) fn router<S>() -> Router<S>
where
    S: Send + Sync + Clone + 'static,
{
    Router::new().route("/warframe.apk", get(apk))
}

async fn apk() -> impl IntoResponse {
    const APK: &[u8] = b"NO-APK"; //include_bytes!("apk_modified.apk");
    let mime: Mime = "application/vnd.android.package-archive".parse().unwrap();

    (TypedHeader::<ContentType>(mime.into()), APK)
}
