use axum::{extract::FromRef, Router};

use crate::data::{AppState, HExports};

mod apk;
mod data;
mod user;

pub fn router<S>() -> Router<S>
where
    S: Send + Sync + Clone + 'static,
    HExports: FromRef<S>,
    AppState: FromRef<S>,
{
    Router::new()
        .nest("/apk", apk::router())
        .nest("/data", data::router())
        .nest("/user", user::router())
}
