use axum::http::Method;
use clap::Parser;
use clap_port_flag::Port;
use tower_http::cors::{Any, CorsLayer};
use tracing::info;
use warframe_web::{data::AppState, routes::router};

#[derive(Debug, Parser)]
struct Cli {
    #[clap(flatten)]
    port: Port,

    #[clap(long)]
    access_control_allow_origin: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::fmt::init();

    let args = Cli::parse();
    let listener = args.port.bind()?;
    let addr = listener.local_addr()?;

    info!(?addr, "Server listening");

    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST])
        .allow_origin(Any);

    axum::Server::from_tcp(listener)?
        .serve(
            router()
                .layer(cors)
                .with_state(AppState::new().await)
                .into_make_service(),
        )
        .await
        .unwrap();

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_test() {
        assert_eq!(42, 42);
    }
}
