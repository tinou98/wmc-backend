use crate::parser::{
    customs::Customs,
    drones::Drones,
    flavours::Flavours,
    fusion_bundles::FusionBundles,
    gears::Gears,
    keys::Keys,
    manifest::Manifest,
    recipes::Recipes,
    relic_arcanes::RelicArcanes,
    ressources::Resources,
    sentinels::Sentinels,
    sortie_rewards::{ExportRailjack, Nightwave, Others, SortieRewards},
    upgrades::{ModSets, Upgrades},
    warframes::Warframes,
    weapons::Weapons,
};

use serde::Deserialize;

use std::iter::Sum;
use std::ops::AddAssign;

#[derive(Default, Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ExportSlice {
    #[serde(default)]
    pub(crate) manifest: Manifest,

    #[serde(default)]
    pub(crate) export_recipes: Recipes,

    #[serde(default)]
    pub(crate) export_warframes: Warframes,

    #[serde(default)]
    pub(crate) export_weapons: Weapons,

    #[serde(default)]
    pub(crate) export_upgrades: Upgrades,

    #[serde(default)]
    pub(crate) export_mod_set: ModSets,

    #[serde(default)]
    pub(crate) export_sortie_rewards: SortieRewards,

    #[serde(default)]
    pub(crate) export_nightwave: Option<Nightwave>,

    #[serde(default)]
    pub(crate) export_railjack: ExportRailjack,

    #[serde(default)]
    pub(crate) export_other: Others,

    #[serde(default)]
    pub(crate) export_sentinels: Sentinels,

    #[serde(default)]
    pub(crate) export_resources: Resources,

    #[serde(default)]
    pub(crate) export_drones: Drones,

    #[serde(default)]
    pub(crate) export_keys: Keys,

    #[serde(default)]
    pub(crate) export_relic_arcane: RelicArcanes,

    #[serde(default)]
    pub(crate) export_gear: Gears,

    #[serde(default)]
    pub(crate) export_flavour: Flavours,

    #[serde(default)]
    pub(crate) export_customs: Customs,

    #[serde(default)]
    pub(crate) export_fusion_bundles: FusionBundles,
}

fn merge_option<T>(lhs: &mut Option<T>, rhs: Option<T>) {
    match (&lhs, &rhs) {
        (Some(_), Some(_)) => panic!("Unable to merge, both value are set"),
        (Some(_), None) => (), // lhs already contain the value
        (None, _) => *lhs = rhs,
    }
}

impl AddAssign for ExportSlice {
    fn add_assign(&mut self, rhs: Self) {
        self.export_recipes.extend(rhs.export_recipes.into_iter());
        self.manifest.extend(rhs.manifest.into_iter());
        self.export_warframes
            .extend(rhs.export_warframes.into_iter());
        self.export_weapons.extend(rhs.export_weapons.into_iter());
        self.export_upgrades.extend(rhs.export_upgrades.into_iter());
        self.export_mod_set.extend(rhs.export_mod_set.into_iter());
        self.export_sortie_rewards
            .extend(rhs.export_sortie_rewards.into_iter());
        self.export_other.extend(rhs.export_other.into_iter());
        self.export_railjack
            .nodes
            .extend(rhs.export_railjack.nodes.into_iter());
        merge_option(&mut self.export_nightwave, rhs.export_nightwave);
        self.export_sentinels
            .extend(rhs.export_sentinels.into_iter());
        self.export_resources
            .extend(rhs.export_resources.into_iter());
        self.export_drones.extend(rhs.export_drones.into_iter());
        self.export_keys.extend(rhs.export_keys.into_iter());
        self.export_relic_arcane
            .extend(rhs.export_relic_arcane.into_iter());
        self.export_gear.extend(rhs.export_gear.into_iter());
        self.export_flavour.extend(rhs.export_flavour.into_iter());
        self.export_customs.extend(rhs.export_customs.into_iter());
        self.export_fusion_bundles
            .extend(rhs.export_fusion_bundles.into_iter());
    }
}

impl Sum for ExportSlice {
    fn sum<I: Iterator<Item = ExportSlice>>(iter: I) -> Self {
        iter.fold(ExportSlice::default(), |mut acc, v| {
            acc += v;
            acc
        })
    }
}
