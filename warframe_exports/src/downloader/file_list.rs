use super::ExportSlice;
use crate::ETagInfo;

use futures::prelude::*;
use lzma_rs::lzma_decompress;
use reqwest::{header::IF_NONE_MATCH, Client};

use std::{convert::TryFrom, io::Cursor};

pub struct FileList {
    pub(crate) etag: ETagInfo,

    list: Vec<String>,
}

impl FileList {
    pub async fn new(
        client: &Client,
        lang: Language,
        etag: Option<&str>,
    ) -> reqwest::Result<Option<Self>> {
        let url: &str = lang.get_url();

        let req = client.get(url);
        let req = if let Some(etag) = etag {
            req.header(IF_NONE_MATCH, etag.to_string())
        } else {
            req
        };

        let response = req.send().await?;

        if response.status() == reqwest::StatusCode::NOT_MODIFIED {
            return Ok(None);
        }

        let etag = ETagInfo::try_from(response.headers()).expect("Can't build ETag info");
        let body = response.bytes().await?;

        let mut rep = Vec::new();
        lzma_decompress(&mut Cursor::new(&body), &mut Cursor::new(&mut rep))
            .expect("File should be an LZMA file");

        let string = String::from_utf8(rep).unwrap();
        let list: Vec<String> = string.split("\r\n").map(|e| e.to_string()).collect();

        Ok(Some(FileList { list, etag }))
    }

    pub async fn download_all(&self, client: &Client) -> reqwest::Result<Vec<ExportSlice>> {
        let base_url =
            reqwest::Url::parse("https://content.warframe.com/PublicExport/Manifest/").unwrap();

        self.list
            .iter()
            .cloned()
            .map(|url| {
                let url = base_url.join(&url).unwrap();
                async move {
                    let txt = client
                        .get(url.clone())
                        .send()
                        .await?
                        .text()
                        .await?
                        .replace('\n', "\\n")
                        .replace('\r', "\\r");

                    Ok(serde_json::from_str(&txt)
                        .unwrap_or_else(|err| panic!("Error in {url}: {err}")))
                }
            })
            .collect::<stream::FuturesUnordered<_>>()
            .try_collect::<Vec<ExportSlice>>()
            .await
    }
}

#[allow(dead_code)]
pub enum Language {
    En,
    Fr,
}

impl Language {
    fn get_url(&self) -> &'static str {
        match self {
            Language::En => "https://content.warframe.com/PublicExport/index_en.txt.lzma",
            Language::Fr => "https://content.warframe.com/PublicExport/index_en.txt.lzma",
        }
    }
}
