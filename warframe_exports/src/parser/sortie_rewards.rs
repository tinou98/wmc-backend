use crate::utils::Rarity;
use serde::{Deserialize, Serialize};

pub type SortieRewards = Vec<SortieReward>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SortieReward {
    pub reward_name: String,
    pub rarity: Rarity,
    pub tier: u8,
    pub item_count: u32,
    pub probability: f32,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Nightwave {
    pub affiliation_tag: String,
    pub challenges: Vec<Challenge>,
    pub rewards: Vec<Reward>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Challenge {
    pub unique_name: String,
    pub name: String,
    pub description: String,
    pub standing: i64,
    pub required: i64,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Reward {
    pub unique_name: String,
    pub item_count: Option<i64>,
    pub name: Option<String>,
    pub description: Option<String>,
    #[serde(default)]
    pub components: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct ExportRailjack {
    pub nodes: Vec<Node>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Node {
    pub unique_name: String,
    pub name: String,
}

pub type Others = Vec<Other>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Other {
    pub unique_name: String,
    pub name: String,
    pub description: String,

    #[serde(default)]
    pub exclude_from_codex: bool,
}
