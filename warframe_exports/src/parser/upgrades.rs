use crate::utils::Rarity;
use serde::{Deserialize, Serialize};
use warframe_shared::Polarity;

pub type Upgrades = Vec<Upgrade>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Upgrade {
    pub unique_name: String,
    pub name: String,

    pub polarity: Polarity,
    pub rarity: Rarity,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
    pub base_drain: i32,
    pub fusion_limit: u16,

    pub compat_name: Option<String>,

    #[serde(rename = "type")]
    pub ty: Option<String>,

    #[serde(default)]
    #[serde(with = "flatten_array")]
    pub description: Option<String>,

    #[serde(default)]
    #[serde(with = "flatten_array")]
    pub tags: Option<String>,

    #[serde(default)]
    pub is_utility: bool,

    #[serde(default)]
    pub level_stats: Vec<Stats>,

    pub mod_set: Option<String>,
    pub mod_set_values: Option<[f32; 2]>,

    pub subtype: Option<String>,

    #[serde(default)]
    pub upgrade_entries: Vec<UpgradeEntries>,
    #[serde(default)]
    pub available_challenges: Vec<Challenge>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct UpgradeEntries {
    prefix_tag: String,
    suffix_tag: String,
    tag: String,
    upgrade_values: Vec<UpgradeValue>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct UpgradeValue {
    loc_tag: Option<String>,
    value: f32,

    #[serde(default)]
    reverse_value_symbol: bool,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Challenge {
    pub full_name: String,
    pub description: String,
    pub complications: Vec<Complication>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Complication {
    pub full_name: String,
    pub description: String,
    pub override_tag: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Stats {
    #[serde(default)]
    stats: Vec<String>,
}

mod flatten_array {
    use serde::{Deserialize, Deserializer, Serialize, Serializer};
    type ArrayType = Option<[String; 1]>;
    type PlainType = Option<String>;

    pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<PlainType, D::Error> {
        ArrayType::deserialize(deserializer).map(|e| e.map(|[e]| e))
    }

    pub fn serialize<S: Serializer>(value: &PlainType, serializer: S) -> Result<S::Ok, S::Error> {
        let value = value.as_ref().map(|e| [e]);
        value.serialize(serializer)
    }
}

pub type ModSets = Vec<ModSet>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct ModSet {
    pub unique_name: String,

    pub num_upgrades_in_set: u8,
    pub stats: Vec<String>,

    #[serde(default)]
    pub buff_set: bool,
}
