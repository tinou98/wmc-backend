use crate::{Item, ItemKind, XPItem};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

pub type Sentinels = Vec<Sentinel>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Sentinel {
    pub unique_name: String,
    pub name: String,
    pub description: Option<String>,

    pub health: u16,
    pub shield: u16,
    pub armor: u16,
    pub stamina: u16,
    pub power: u16,

    pub codex_secret: bool,

    #[serde(default)]
    pub exclude_from_codex: bool,

    pub mastery_req: Option<u8>,
    pub product_category: String,
}

impl Item for Sentinel {
    fn unique_name(&self) -> &str {
        &self.unique_name
    }

    fn name(&self) -> Cow<str> {
        self.name.as_str().into()
    }

    fn description(&self) -> Cow<str> {
        self.description.as_deref().unwrap_or_default().into()
    }

    fn kind(&self) -> ItemKind {
        ItemKind::Sentinel
    }
}

impl XPItem for Sentinel {
    fn as_item(&self) -> &dyn Item {
        self
    }
}
