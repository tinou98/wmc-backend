use serde::{Deserialize, Serialize};

pub type Regions = Vec<Region>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Region {
    pub unique_name: String,
    pub name: String,

    pub system_index: u8,
    pub system_name: String,

    pub node_type: u8,
    pub mastery_req: u8,
    pub mission_index: u8,
    pub faction_index: u8,

    pub min_enemy_level: u8,
    pub max_enemy_level: u8,
}
