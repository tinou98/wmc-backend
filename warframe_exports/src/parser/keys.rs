use serde::{Deserialize, Serialize};

pub type Keys = Vec<Key>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Key {
    pub unique_name: String,
    pub name: String,
    pub description: String,
    pub parent_name: String,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
}
