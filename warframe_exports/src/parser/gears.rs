use crate::Item;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

pub type Gears = Vec<Gear>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Gear {
    pub unique_name: String,
    pub name: String,

    pub description: String,
    pub parent_name: String,

    pub codex_secret: bool,
}

impl Item for Gear {
    fn unique_name(&self) -> &str {
        &self.unique_name
    }

    fn name(&self) -> Cow<str> {
        self.name.as_str().into()
    }

    fn description(&self) -> Cow<str> {
        self.description.as_str().into()
    }

    fn kind(&self) -> crate::ItemKind {
        crate::ItemKind::Other
    }
}
