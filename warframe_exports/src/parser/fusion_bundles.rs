use serde::{Deserialize, Serialize};

pub type FusionBundles = Vec<ExportFusionBundle>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ExportFusionBundle {
    pub unique_name: String,
    pub description: String,

    pub codex_secret: bool,
    pub fusion_points: u16,
}
