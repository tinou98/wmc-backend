use serde::{Deserialize, Serialize};

pub type Drones = Vec<Drone>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Drone {
    pub unique_name: String,
    pub name: String,
    pub description: String,

    pub bin_count: u8,
    pub bin_capacity: u8,

    pub fill_rate: f32,
    pub durability: u8,
    pub repair_rate: u8,
    pub codex_secret: bool,
    pub capacity_multiplier: [u8; 4],
    pub specialities: [(); 0],
}
