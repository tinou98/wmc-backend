use serde::{Deserialize, Serialize};

pub type Flavours = Vec<Flavour>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Flavour {
    pub unique_name: String,
    pub name: String,
    pub description: String,

    #[serde(default)]
    pub hex_colours: Vec<HexColour>,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct HexColour {
    pub value: String, // Hex color
}
