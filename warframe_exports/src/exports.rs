use crate::{
    downloader::{ExportSlice, ExportsInfo},
    parser::{
        customs::Customs,
        drones::Drones,
        flavours::Flavours,
        fusion_bundles::FusionBundles,
        gears::Gears,
        keys::Keys,
        manifest::Manifest,
        recipes::Recipes,
        relic_arcanes::RelicArcanes,
        ressources::Resources,
        sentinels::Sentinels,
        sortie_rewards::{ExportRailjack, Nightwave, Others, SortieRewards},
        upgrades::{ModSets, Upgrades},
        warframes::Warframes,
        weapons::Weapons,
    },
    recipes::Recipe,
    relic_arcanes::RelicArcane,
    warframes::Warframe,
    ETagInfo, Item, XPItem,
};
use serde::{Deserialize, Serialize};
use serde_json::{Deserializer, Serializer};
use std::{
    error::Error,
    fs::File,
    io::{ErrorKind::NotFound, Result as IoResult},
    path::Path,
};
use tracing::warn;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Exports {
    pub(crate) manifest: Manifest,
    pub(crate) export_recipes: Recipes,
    pub(crate) export_warframes: Warframes,
    pub(crate) export_weapons: Weapons,
    pub(crate) export_upgrades: Upgrades,
    pub(crate) export_mod_set: ModSets,
    pub(crate) export_sortie_rewards: SortieRewards,
    pub(crate) export_nightwave: Nightwave,
    pub(crate) export_railjack: ExportRailjack,
    pub(crate) export_other: Others,
    pub(crate) export_sentinels: Sentinels,
    pub(crate) export_resources: Resources,
    pub(crate) export_drones: Drones,
    pub(crate) export_keys: Keys,
    pub(crate) export_relic_arcane: RelicArcanes,
    pub(crate) export_gear: Gears,
    pub(crate) export_flavour: Flavours,
    pub(crate) export_customs: Customs,
    pub(crate) export_fusion_bundles: FusionBundles,
}

impl Exports {
    pub async fn new<P>(path: P) -> IoResult<Self>
    where
        P: AsRef<Path> + Send + 'static,
    {
        let etag: Option<(ETagInfo, Deserializer<_>)> = match File::open(&path) {
            Ok(f) => {
                let mut de = Deserializer::from_reader(f);
                ETagInfo::deserialize(&mut de).ok().map(|info| (info, de))
            }

            Err(e) if e.kind() == NotFound => None,
            Err(e) => return Err(e),
        };

        let export_info = ExportsInfo::fetch(etag.as_ref().map(|e| &e.0))
            .await
            .unwrap();

        let exports = match (export_info, etag) {
            (Some(e), reader) => {
                drop(reader);
                match File::create(&path) {
                    Err(err) => {
                        warn!(
                            path = ?path.as_ref(),
                            err = (&err as &dyn Error),
                            "Unable to save Exports",
                        )
                    }
                    Ok(file) => {
                        let mut se = Serializer::new(file);
                        e.etag.serialize(&mut se).unwrap();
                        e.export.serialize(&mut se).unwrap();
                    }
                }

                e.export
            }
            (None, Some((_, mut de))) => Exports::deserialize(&mut de).unwrap(),
            (None, None) => {
                panic!("No previous version available, but downloader report already up to date")
            }
        };

        Ok(exports)
    }

    pub fn recipe_for<'a>(&'a self, name: &'a str) -> impl Iterator<Item = &'a Recipe> {
        // todo!(performance)
        self.export_recipes
            .iter()
            .filter(move |r| r.result_type == name)
    }
    /*
        pub fn recipe(&self, _name: &str) -> Option<Recipe> {
            todo!()
        }
    */
    /*
        pub fn items(&self) -> Vec<&dyn Item> {
            todo!()
        }
    */

    pub fn items(&self) -> impl Iterator<Item = &dyn Item> {
        std::iter::empty()
            .chain(self.export_recipes.iter().map(as_item))
            .chain(self.export_warframes.iter().map(as_item))
            .chain(self.export_weapons.iter().map(as_item))
            //.chain(self.export_upgrades.iter().map(as_item))
            //.chain(self.export_mod_set.iter().map(as_item))
            //.chain(self.export_sortie_rewards.iter().map(as_item))
            //.chain(self.export_nightwave.iter().map(as_item))
            //.chain(self.export_railjack.iter().map(as_item))
            //.chain(self.export_other.iter().map(as_item))
            //.chain(self.export_sentinels.iter().map(as_item))
            .chain(self.export_resources.iter().map(as_item))
            //.chain(self.export_drones.iter().map(as_item))
            //.chain(self.export_keys.iter().map(as_item))
            //.chain(self.export_relic_arcane.iter().map(as_item))
            .chain(self.export_gear.iter().map(as_item))
        //.chain(self.export_flavour.iter().map(as_item))
        //.chain(self.export_customs.iter().map(as_item))
        //.chain(self.export_fusion_bundles.iter().map(as_item))
    }

    pub fn xp_items(&self) -> impl Iterator<Item = &dyn XPItem> {
        std::iter::empty()
            .chain(self.export_warframes.iter().map(as_xp_item))
            .chain(self.export_weapons.iter().map(as_xp_item))
    }

    pub fn par_xp_items(&self) -> impl rayon::iter::IndexedParallelIterator<Item = &dyn XPItem> {
        use rayon::prelude::*;
        rayon::iter::empty()
            .chain(self.export_warframes.par_iter().map(as_xp_item))
            .chain(self.export_weapons.par_iter().map(as_xp_item))
    }

    /*
    pub fn xp_items_by_name_like(&self, name: &str) -> Vec<&dyn XPItem> {
        todo!() //RawXPItem::by_name_like(name, &self.db).await.unwrap()
    }
    */

    pub fn warframe(&self) -> &Vec<Warframe> {
        &self.export_warframes
    }

    pub fn get_image(&self, item: &dyn Item) -> Option<&str> {
        let unique_name = item.unique_name();
        self.manifest
            .iter()
            .find(|el| el.unique_name == unique_name)
            .map(|el| el.texture_location.as_str())
    }

    pub fn relic_for<'a>(&'a self, name: &'a str) -> impl Iterator<Item = &RelicArcane> + 'a {
        self.export_relic_arcane.iter().filter(move |relic| {
            relic
                .relic_rewards
                .iter()
                .any(move |reward| reward.reward_name.replace("/StoreItems", "") == name)
        })
    }
}

impl std::convert::TryFrom<ExportSlice> for Exports {
    type Error = ();

    fn try_from(slices: ExportSlice) -> Result<Self, Self::Error> {
        Ok(Self {
            manifest: slices.manifest,
            export_recipes: slices.export_recipes,
            export_warframes: slices.export_warframes,
            export_weapons: slices.export_weapons,
            export_upgrades: slices.export_upgrades,
            export_mod_set: slices.export_mod_set,
            export_sortie_rewards: slices.export_sortie_rewards,
            export_nightwave: slices.export_nightwave.ok_or(())?,
            export_railjack: slices.export_railjack,
            export_other: slices.export_other,
            export_sentinels: slices.export_sentinels,
            export_resources: slices.export_resources,
            export_drones: slices.export_drones,
            export_keys: slices.export_keys,
            export_relic_arcane: slices.export_relic_arcane,
            export_gear: slices.export_gear,
            export_flavour: slices.export_flavour,
            export_customs: slices.export_customs,
            export_fusion_bundles: slices.export_fusion_bundles,
        })
    }
}

fn as_xp_item<I: XPItem>(item: &I) -> &dyn XPItem {
    item
}

fn as_item<I: Item>(item: &I) -> &dyn Item {
    item
}
