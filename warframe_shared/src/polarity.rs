use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "UPPERCASE")]
#[serde(deny_unknown_fields)]
pub enum Polarity {
    #[serde(rename = "AP_DEFENSE")]
    Defense,

    #[serde(rename = "AP_ATTACK")]
    Attack,

    #[serde(rename = "AP_TACTIC")]
    Tactic,

    #[serde(rename = "AP_POWER")]
    Power,

    #[serde(rename = "AP_PRECEPT")]
    Precept,

    #[serde(rename = "AP_UNIVERSAL")]
    Universal,

    #[serde(rename = "AP_UMBRA")]
    Umbra,

    #[serde(rename = "AP_WARD")]
    Ward,
}
